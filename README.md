# Docker Django

## Управление

### Запуск

```bash
docker-compose up --build -d api
```

### Запуск тестов

```bash
docker-compose up --build autotests
```

### Документация

После запуска сервера доступна по адресу [http://127.0.0.1:8000/swagger]
