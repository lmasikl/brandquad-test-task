import re

import maya
import requests

from tqdm import tqdm

from .models import Log


class Writer:

    def __init__(self, size=None):
        super().__init__()
        self.size = size
        self.buffer = []
        # ip datetime method uri code size
        self.row_pattern = r'([(\d\.)]+) - - \[(\d{2}\/\w{3}\/\d{4}:\d{2}:\d{2}:\d{2} \+\d{4})\] "(\w{3}) ([(\/\w+\/*)]+).{9}" (\d{3}) (\d+)'

    def write(self, data):
        for value in data:
            instance = self.make_instance(value)
            is_buffer_ready = (self.size is None or self.size > len(self.buffer))
            if instance is not None and is_buffer_ready:
                self.buffer.append(instance)

            if self.size is not None and len(self.buffer) == self.size:
                self.write_buffer()

    def write_buffer(self):
        Log.objects.bulk_create(self.buffer)
        self.buffer = []

    def make_instance(self, value):

        regexp = re.match(self.row_pattern, value.strip())
        if not regexp:
            return None

        group = regexp.groups()
        return Log(
            ip=group[0],
            datetime=maya.parse(group[1].replace(':', ' ', 1)).datetime(),
            method=group[2],
            uri=group[3],
            code=int(group[4]),
            size=int(group[5]),
        )


class Loader:

    def __init__(self):
        self.block_size = 4096
        self.remainder = ''
        self.writer = Writer()

    def load(self, url='http://www.almhuette-raith.at/apache-log/access.log'):
        response = requests.get(url, stream=True)
        total_size_in_bytes = int(response.headers.get('content-length', 0))
        stream = response.iter_content(self.block_size)
        self._load(stream, total_size_in_bytes)

    def _load(self, stream, total_size):
        progress_bar = tqdm(total=total_size, unit='iB', unit_scale=True)
        for data in stream:
            self.read_chunk(data)
            progress_bar.update(len(data))

        if self.writer.buffer:
            buffer_len = len(self.writer.buffer)
            self.writer.write_buffer()
            progress_bar.update(buffer_len)

        progress_bar.close()

    def read_chunk(self, chunk):
        data = chunk.decode()
        data = self.remainder + data
        is_chunk_has_remainder = data[-1] != '\n'
        data = data.split('\n')
        if is_chunk_has_remainder:
            self.remainder = data.pop()

        self.writer.write(data)
