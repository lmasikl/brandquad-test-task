from collections import OrderedDict

from django.db.models import Count, Sum, Q

from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response


class LogPagination(LimitOffsetPagination):
    def __init__(self):
        self.meta = None

    def paginate_queryset(self, queryset, request, view=None):
        self.set_meta(queryset)
        return super().paginate_queryset(queryset, request, view=view)

    def set_meta(self, queryset):
        aggregators = {
            'total_size': Sum('size'),
            'ips_count': Count('ip', distinct=True),
        }

        aggregators.update(dict(
            (f'{m}_count', Count('method', filter=Q(method=m))) for m in queryset.distinct().values_list('method', flat=True)
        ))

        self.meta = queryset.aggregate(**aggregators)

        self.meta['top_ten'] = queryset.values('ip').order_by('ip').annotate(
            count=Count('ip')
        ).order_by('-count')[:10].values_list('ip', 'count')

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.count),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('meta', self.meta),
            ('results', data)
        ]))
