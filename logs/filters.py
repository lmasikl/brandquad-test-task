import django_filters

from .models import Log


class LogFilter(django_filters.FilterSet):

    class Meta:
        model = Log
        fields = {
            'datetime': ['exact', 'gt', 'lt'],
            'method': ['exact', 'in'],
            'code': ['exact', 'in'],
            'uri': ['exact', 'contains'],
            'ip': ['exact', 'contains'],
        }
