from django.core.management.base import BaseCommand
from django.db import connection

from logs.loaders import Loader


class Command(BaseCommand):

    help = 'Download and process Apache logs'

    def add_arguments(self, parser):
        parser.add_argument(
            '-u', '--url', dest="url", nargs='+', type=str, help="Logs url"
        )

    def handle(self, *args, **options):
        urls = options.get('url')
        if not urls:
            urls = ['http://www.almhuette-raith.at/apache-log/access.log']

        for url in urls:
            loader = Loader()
            loader.load(url)
