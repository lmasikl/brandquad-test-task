from django.db import models


class Log(models.Model):
    ip = models.GenericIPAddressField()
    datetime = models.DateTimeField()
    method = models.CharField(max_length=8)
    uri = models.CharField(max_length=256)
    code = models.PositiveIntegerField()
    size = models.PositiveIntegerField(default=0)
