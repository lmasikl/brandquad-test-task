import logging

from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from rest_framework.viewsets import ReadOnlyModelViewSet

from drf_renderer_xlsx.mixins import XLSXFileMixin
from drf_renderer_xlsx.renderers import XLSXRenderer

from django_filters.rest_framework import DjangoFilterBackend

from .serializers import LogSerializer

from .filters import LogFilter
from .models import Log
from .pagination import LogPagination
from rest_framework.pagination import LimitOffsetPagination


logger = logging.getLogger(__name__)


class LogViewSet(XLSXFileMixin, ReadOnlyModelViewSet):
    """
    REST API for log.
    """
    queryset = Log.objects.all()
    serializer_class = LogSerializer
    pagination_class = LogPagination
    filter_backends = (DjangoFilterBackend, )
    filterset_class = LogFilter
    renderer_classes = (JSONRenderer, XLSXRenderer, TemplateHTMLRenderer)
    template_name = 'logs.html'
    filename = 'log-export.xlsx'
