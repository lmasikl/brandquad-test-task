import datetime
import pytest
import uuid

from rest_framework.test import APIClient

from logs.models import Log


@pytest.fixture(autouse=True)
def enable_db_access_for_all_tests(db):
    pass


@pytest.fixture
def log():
    return Log.objects.create(
        ip='127.0.0.1',
        datetime=datetime.datetime.now(),
        method='GET',
        uri='/admin/',
        code=200,
        size=4096,
    )


@pytest.fixture
def test_logs():
    return [
        b'109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-"\n',
        b'46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-"\n',
    ]
