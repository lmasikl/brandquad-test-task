import time
import datetime
import json
import sys
from io import BytesIO

from django.urls import reverse

from logs.loaders import Loader
from logs.models import Log


def test_log_api(client, log):
    list_url = reverse('v1:log-list')
    response = client.get(list_url + '?format=json', format='json')
    assert response.status_code == 200, response.json()
    data = response.json()
    for key in ('next', 'previous', 'meta', 'results', 'count'):
        assert key in data

    for key in ('top_ten', 'ips_count', 'GET_count', 'total_size'):
        assert key in data['meta']

    assert len(data['results']) == 1
    assert len(data['meta']['top_ten']) == 1

    response = client.get(list_url + '?format=html', format='html')
    assert response.status_code == 200

    response = client.get(list_url + '?format=xlsx', format='xlsx')
    assert response.status_code == 200

def test_loader(test_logs):
    text = b''.join(test_logs)
    stream = BytesIO(text)
    loader = Loader()
    loader._load(stream, sys.getsizeof(text))
    assert Log.objects.all().count() == 2
