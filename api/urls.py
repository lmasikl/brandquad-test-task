from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include, url

from rest_framework import permissions
from rest_framework.routers import DefaultRouter
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from logs.views import LogViewSet


schema_view = get_schema_view(
    openapi.Info(
        title="Log API",
        default_version='v1',
        description="Brandquad test task",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

router = DefaultRouter()
router.register(r'logs', LogViewSet)

urlpatterns = [
    url(r'^v1/', include((router.urls, 'v1'), namespace='v1')),
    url(
        r'^swagger(?P<format>\.json|\.yaml)$',
        schema_view.without_ui(cache_timeout=0),
        name='schema-json'
    ),
    url(
        r'^swagger/$',
        schema_view.with_ui('swagger', cache_timeout=0),
        name='schema-swagger-ui'
    ),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
