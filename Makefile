default: run

run:
	@docker-compose up --build -d api

logs:
	@docker-compose logs -f api

test:
	@docker-compose up --build autotests

makemigrations:
	@pipenv run ./manage.py makemigrations

migrate:
	@pipenv run ./manage.py migrate

collectstatic:
	@pipenv run ./manage.py collectstatic

loadlog:
	@pipenv run ./manage.py loadlog

run-dev:
	@pipenv run ./manage.py runserver

test-dev:
	@pipenv run pytest
